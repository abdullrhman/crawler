const CrawlE = require('crawl-e/v0.1.3')
const moment = require('moment')

const crawlE = new CrawlE({
  crawler: { is_booking_link_capable: false },
  cinemas: [
    {
      name: 'Kingaroy Cinema',
      address: '4 Short St Kingaroy QLD 4610 Australia',
      website: 'https://kingaroycinema.com/',
      phone: "41622336",
    }
  ],
  showtimes: {
    url: "https://kingaroycinema.com/film/",
    movies: {
      box: 'article',
      title: 'h2 a',
      date:".poster_date_format h4",
      start_at:".date_time_btn",
      bookingLink:'h2 a @href'
    }
  }

})

crawlE.crawl()